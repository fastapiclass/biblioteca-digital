from models.categoria import Categoria as CategoriaModel
from models.libro import Libro as LibroModel
from schemas.categoria import Categoria

class CategoriaService():
    def __init__(self, db) -> None:
        self.db = db

    def get_categoria(self, id):
        result = self.db.query(CategoriaModel).filter(CategoriaModel.id == id).first()
        return result

    def get_all_categorias(self):
        result = self.db.query(CategoriaModel).all()
        return result

    def create_categoria(self, categoria: Categoria) -> None:
        newCategoria = CategoriaModel(**categoria.model_dump())
        self.db.add(newCategoria)
        self.db.commit()
    
    def update_categoria(self, id: int, data: Categoria):
        resultCategoria = self.db.query(CategoriaModel).filter(CategoriaModel.id == id).first()
        resultLibro = self.db.query(LibroModel).filter(LibroModel.categoria == resultCategoria.categoria).all()
        if not resultLibro:
            resultCategoria.categoria = data.categoria
            self.db.commit()
        else:
            resultCategoria.categoria = data.categoria
            for libro in resultLibro:
                    libro.categoria = resultCategoria.categoria
            self.db.commit()

    def delete_categoria(self, id: int) -> None:
        self.db.query(CategoriaModel).filter(CategoriaModel.id == id).delete()
        self.db.commit()
            