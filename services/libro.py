from schemas.libro import Libro
from models.libro import Libro as LibroModel
from models.categoria import Categoria as CategoriaModel

class LibroService():
    def __init__(self, db) -> None:
        self.db = db
    
    def get_libros(self):
        result = self.db.query(LibroModel).all()
        return result

    def get_libro(self, codigo: int):
        result = self.db.query(LibroModel).filter(LibroModel.codigo == codigo).first()
        return result

    def get_libros_by_categoria(self, categoria: str):
        result = self.db.query(LibroModel).filter(LibroModel.categoria == categoria).all()
        return result

    def create_libro(self, libro: Libro):
        newLibro = LibroModel(**libro.model_dump())
        self.db.add(newLibro)
        self.db.commit()
    
    def delete_libro(self, codigo: int):
        self.db.query(LibroModel).filter(LibroModel.codigo == codigo).delete()
        self.db.commit()
    
    def update_libro(self, codigo: int, libro: Libro) -> None:
        resultLibro = self.db.query(LibroModel).filter(LibroModel.codigo == codigo).first()
        resultLibro.titulo = libro.titulo
        resultLibro.autor = libro.autor
        resultLibro.año = libro.año
        resultLibro.categoria = libro.categoria
        resultLibro.numeroPaginas = libro.numeroPaginas
        self.db.commit()

    def get_categoria_en_libro(self, libro: Libro):
        resultCategoria = self.db.query(CategoriaModel).filter(CategoriaModel.categoria == libro.categoria).first()
        return resultCategoria