from fastapi.security import HTTPBearer
from fastapi import Request, HTTPException
from utils.jwt_manager import validate_token
from config.database import Session
from models.user import User as UserModel

class JWTBearer(HTTPBearer):
	async def __call__(self, request: Request):
		auth = await super().__call__(request) 
		data = validate_token(auth.credentials)
		db = Session()
		resultemail = db.query(UserModel).filter(UserModel.email).all()
		for email in resultemail:
			if data['email'] != email:
				raise HTTPException(status_code=403, detail="Credenciales invalidas")