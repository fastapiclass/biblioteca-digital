from fastapi import APIRouter
from utils.jwt_manager import create_token
from fastapi.responses import JSONResponse
from config.database import Session
from models.user import User as UserModel
from schemas.user import User

user_router = APIRouter()

@user_router.post('/authentication/registro', tags=['Authentication'], response_model=dict, status_code=200)
def create_user(usuario: User) -> dict:
	db = Session()
	new_user = UserModel(**usuario.model_dump())
	db.add(new_user)
	db.commit()
	return JSONResponse(content={"message":"Registro de usuario exitosamente."}, status_code=200)

@user_router.post('/authentication/login', tags=['Authentication'])
def login(user: User) -> dict:
	db = Session()
	resultemail = db.query(UserModel).filter(UserModel.email == user.email, UserModel.password == user.password).first()
	if resultemail:
		token: str = create_token(user.dict())
		return JSONResponse(status_code=200, content=token)
	else:
		return JSONResponse(status_code=404, content={"message":"Esas licencias no han sido registradas."})