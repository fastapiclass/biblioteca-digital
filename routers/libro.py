from typing import List
from config.database import Session
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi import Path, Query
from fastapi import APIRouter
from schemas.libro import Libro
from services.libro import LibroService

#, dependencies=[Depends(JWTBearer())]

libro_router = APIRouter()

@libro_router.get('/libros', tags=['Libros'], response_model=List[Libro], status_code=200)
def get_libros() -> List[Libro]:
    db = Session()
    result = LibroService(db).get_libros()
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@libro_router.get('/libros/{codigo}', tags=['Libros'], response_model=Libro, status_code=200)
def get_libro(codigo: int = Path(ge=1, le=100)) -> Libro:
    db = Session()
    result = LibroService(db).get_libro(codigo)
    if not result:
        return JSONResponse(status_code=404, content={"message":"No hay registro con ese codigo."})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@libro_router.get('/libros/categoria/', tags=['Libros'], response_model=List[Libro], status_code=200)
def get_libros_by_category(categoria: str = Query(min_length=5, max_length=15)):
    db = Session()
    result = LibroService(db).get_libros_by_categoria(categoria)
    if not result:
        return JSONResponse(status_code=404, content={"message":"No hay libros registrados con esa categoria."})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@libro_router.post('/libros/registrar/', tags=['Libros'], response_model=dict, status_code=200)
def create_libro(libro: Libro) -> dict:
    db = Session()
    resultCategoria = LibroService(db).get_categoria_en_libro(libro)
    if not resultCategoria:
        return JSONResponse(status_code=404,content={"message":"Esa categoria no ha sido registrada."})
    LibroService(db).create_libro(libro)
    return JSONResponse(content={"message":"Libro registrado exitosamente"}, status_code=200)

@libro_router.delete('/libros/eliminar/', tags=['Libros'], response_model=dict, status_code=200)
def delete_libro(codigo: int) -> dict:
    db = Session()
    result = LibroService(db).get_libro(codigo)
    if not result:
        return JSONResponse(status_code=404, content={"message":"No hay registro con ese codigo."})
    LibroService(db).delete_libro(codigo)
    return JSONResponse(content={"message":"Se ha elimiando exitosamente"}, status_code=200)

@libro_router.put('/libros/actualizar/', tags=['Libros'], response_model=dict, status_code=200)
def update_libro(codigo: int, libro: Libro):
    db = Session()
    resultLibro = LibroService(db).get_libro(codigo)
    if not resultLibro:
        return JSONResponse(status_code=404, content={"message":"No hay libro registrado con ese codigo."})
    resultCategoria = LibroService(db).get_categoria_en_libro(libro)
    if not resultCategoria:
        return JSONResponse(status_code=404, content={"message":"No ha sido registrada esa categoria."})
    LibroService(db).update_libro(codigo, libro)
    return JSONResponse(status_code=200, content={"message":"Registro actualizado exitosamente."})