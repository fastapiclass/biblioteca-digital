from typing import List
from config.database import Session
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi import APIRouter
from middleware.jwt_bearer import JWTBearer
from schemas.categoria import Categoria
from services.categoria import CategoriaService
from models.libro import Libro as LibroModel

#, dependencies=[Depends(JWTBearer())]

categoria_router = APIRouter()

@categoria_router.get('/categorias', tags=['Categorias'], response_model=List[Categoria], status_code=200)
def get_all_categorias() -> List[Categoria]:
    db = Session()
    result = CategoriaService(db).get_all_categorias()
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@categoria_router.post('/categorias/agregar/', tags=['Categorias'], response_model=dict, status_code=200)
def create_categoria(categoria: Categoria) -> dict:
    db = Session()
    CategoriaService(db).create_categoria(categoria)
    return JSONResponse(content={"message": "Registro de categoria exitoso"}, status_code=200)

@categoria_router.put('/categorias/actualizar/', tags=['Categorias'], response_model=dict, status_code=200)
def update_categoria(id: int, categoria: Categoria) -> dict:
    db = Session()
    resultCategoria = CategoriaService(db).get_categoria(id)
    if not resultCategoria:
        return JSONResponse(status_code=404, content={"message":"No hay categoria registrada con ese id."})
    resultLibro = db.query(LibroModel).filter(LibroModel.categoria == resultCategoria.categoria).all()
    if resultLibro:
        CategoriaService(db).update_categoria(id, categoria)
        return JSONResponse(status_code=404, content={"message":"Se ha modificado la categoria y los libros con esa categoria."})
    else:
        CategoriaService(db).update_categoria(id, categoria)
        return JSONResponse(status_code=404, content={"message":"Se ha modificado la categoria"})

@categoria_router.delete('/categorias/eliminar/', tags=['Categorias'], response_model=dict, status_code=200)
def delete_categoria(id: int) -> dict:
    db = Session()
    resultCategoria = CategoriaService(db).get_categoria(id)
    if not resultCategoria:
        return JSONResponse(status_code=404, content={"message":"No hay categoria registrada con ese id."})
    resultLibro = db.query(LibroModel).filter(LibroModel.categoria == resultCategoria.categoria).all()
    if resultLibro:
        return JSONResponse(status_code=404, content={"message":"Tiene libros registrados con esa categoria."})
    CategoriaService(db).delete_categoria(id)
    return JSONResponse(status_code=404, content={"message":"Categoria eliminada exitosamente."})
