from pydantic import BaseModel, Field
from typing import Optional

class Categoria(BaseModel):
    id: Optional[int] = None
    categoria: str = Field(min_length = 4, max_length = 15)

    class Config:
        json_schema_extra = {
            "example": {
                "categoria": "Nombre de la categoría"
            }
        }