from typing import Optional
from pydantic import BaseModel, Field

class User(BaseModel):
	id: Optional[int] = None
	email: str = Field(min_length=10, max_length=35)
	password: str = Field(min_length=5, max_length=15)

	class Config:
		json_schema_extra = {
			"example": {
				"email": "...@gmail.com",
				"password": "Contraseña"
			}
		}