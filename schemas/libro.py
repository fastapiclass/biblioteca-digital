from pydantic import BaseModel, Field
from typing import Optional

class Libro(BaseModel):
	codigo: Optional[int] = None
	titulo: str = Field(min_length=2, max_length=35)
	autor : str = Field(min_length=5, max_length=20)
	año: int = Field(le=2024)
	categoria: str = Field(min_length=5, max_length=15)
	numeroPaginas: int = Field(ge=49, le=1500)

	class Config:
		json_schema_extra = {
			"example": {
				"titulo": "Titulo del libro",
				"autor": "Autor del libro",
				"año": 1234,
				"categoria": "Categoría del Libro",
				"numeroPaginas": 123
			}
		}