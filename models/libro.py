from config.database import Base
from sqlalchemy import Column, Integer, String

class Libro (Base):

    __tablename__ = "libros"

    codigo = Column(Integer, primary_key = True, autoincrement = True)
    titulo = Column(String)
    autor = Column(String)
    año = Column(Integer)
    categoria = Column(String)
    numeroPaginas = Column(Integer)