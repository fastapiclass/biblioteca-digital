from config.database import Base
from sqlalchemy import Column, String, Integer

class User (Base):

    __tablename__ = "usuarios"

    id = Column(Integer, primary_key = True, autoincrement = True)
    email = Column(String)
    password = Column(String)