from fastapi import FastAPI
from config.database import engine, Base
from middleware.error_handler import ErrorHandler
from routers.categoria import categoria_router
from routers.libro import libro_router
from routers.user import user_router
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
app.title="Proyecto 3er Parcial Cristian se fue al baño"
app.version="0.0.3"

# Configuración de CORS
origins = [
    "http://localhost:4200",  # URL de tu aplicación Angular en desarrollo
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],  # Permitir todos los métodos (GET, POST, etc.)
    allow_headers=["*"],  # Permitir todas las cabeceras
)

app.add_middleware(ErrorHandler)

Base.metadata.create_all(bind=engine)

# app.include_router(user_router)
app.include_router(categoria_router)
app.include_router(libro_router)